@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
				{{-- <a href="/categories/{{$tops->id}}" class="btn btn-success">Tops</a>
				@if(count($category->titles) > 0)
					@foreach($category->titles as $title)
						{{$asset->name}}
					@endforeach
				@endif --}}

			<div class="col-12">
				
				{{-- same as if admin == 1--}}
				@can('isAdmin')
					<h2 class="text-center" style= "font-family: 'Montserrat', sans-serif; font-weight: 300; text-transform: uppercase;">Product Inventories</h2>
					<div class="text-center d-flex justify-content-center">
						<a href="/categories/create" class="text-center"><i class="fas fa-plus " style="color:black;"></i> <span style="color:black; font-family: 'Montserrat', sans-serif; font-weight: 300; text-transform: uppercase;">Add New Products</span></a>
					</div>
					<br>

					<table class="table">
					<thead class="thead-light text-center">
						<tr>
							<th>Inventory ID</th>
							{{-- <th>Product</th> --}}
							<th>Product Name</th>
							<th>Description</th>
							<th>Size</th>
							<th>Status</th>
							<th>Category</th>
							<th>Actions</th>
						</tr>
					</thead>

					<tbody>
						@foreach($inventories as $inventory)
							@if($asset->id == $inventory->asset_id)
							<tr>
								<td>C{{$asset->category->id}}-A{{$asset->id}}-I{{$inventory->id}}</td>
								{{-- <td><div style="width: 25%;" class="mx-auto ">
									<img src="{{asset($asset->imgPath)}}" class="card-img-top">
								</div></td>  --}}
								<td>{{$inventory->name}}</td>
								<td>{{$asset->description}}</td>
								<td>{{$inventory->size}}</td>
								<td>
									@if($inventory->isAvailable ==1 && $asset->isActive ==1)
										{{"Active"}}

									@else
										{{"Inactive"}}
									@endif
								</td>
								<td>{{$asset->category->name}}</td>
								<td>
									<a href="/inventories/{{$inventory->id}}/edit" class="btn btn-outline-secondary" style="float:right;">Edit</a>
									<form method="POST" action="/inventories/{{$inventory->id}}">
										@csrf
										@method('DELETE')
										@if($inventory->isAvailable == 1)
											<button type="submit"  class="btn btn-outline-danger" style="float:right;">Deactivate</button>
										@else
											<button type="submit" class="btn btn-outline-success" style="float:right;">Reactivate</button>
										@endif
									</form>
								</td>
							</tr>
							@endif
						@endforeach
					</tbody>
				@endcan
			</div>
		</div>
	</div>
@endsection