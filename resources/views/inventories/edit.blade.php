@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-8 offset-2">
			<div class="card">
				<div class="card-header">
					Edit Product
				</div>
				<div class="card-body">
					{{-- error checker --}}
					
					<div>
						@if ($errors->any())
					         <div class="alert alert-danger">
			  		         <div>
			  		         	<button type="button" class="close" data-dismiss="alert">&times;</button>
			  		         </div>
						            <ul>
						                @foreach ($errors->all() as $error)
						                    <li>{{ $error }}</li>
						                @endforeach
						            </ul>
			  		         </div>				     
						@endif
					</div>
					{{-- end of error check --}}
					<form method="POST" action="/inventories/{{$inventory->id}}" enctype="multipart/form-data">
						@csrf
						@method('PUT')
						<div class="form-group">
							<label for="name">Name: </label>
							<input class="form-control" type="text" name="name" id="name" value="{{$inventory->name}}">
						</div>
						<div class="form-group">
							<label for="size">Change to update size: </label>
							<select id="size" name="size">

								<option value="{{$inventory->size}}">Update Size</option>
								<option value="freesize">freesize</option>
								<option value="extra small">extra small</option>
								<option value="small">small</option>
								<option value="medium">medium</option>
								<option value="large">large</option>
								<option value="extra big">extra big</option>
				
							</select>
						</div>

						<button type="submit" class="btn btn-success">
							Edit Product
						</button>							
					</form>
				</div>
			</div>
		</div>
	</div>

@endsection
