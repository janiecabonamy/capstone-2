@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-8 offset-2">
			<div class="card">
				<div class="card-header">
					Edit Category
				</div>
				<div class="card-body">
					{{-- error checker --}}
					
					<div>
						@if ($errors->any())
					         <div class="alert alert-danger">
			  		         <div>
			  		         	<button type="button" class="close" data-dismiss="alert">&times;</button>
			  		         </div>
						            <ul>
						                @foreach ($errors->all() as $error)
						                    <li>{{ $error }}</li>
						                @endforeach
						            </ul>
			  		         </div>
								     
						@endif
					</div>
					{{-- end of error check --}}
					<form method="POST" action="/categories/{{$category->id}}" enctype="multipart/form-data">
						@csrf
						@method('PUT')
						<div class="form-group">
							<label for="name">Name: </label>
							<input class="form-control" type="text" name="name" id="name" value="{{$category->name}}">
						</div>
						
						<div class="form-group">
							<label for="image">Image:</label>
							<input class="form-control" type="file" name="image" id="image">
						</div>

						<button type="submit" class="btn btn-success">
							Edit Category
						</button>							
					</form>
				</div>
			</div>
		</div>
	</div>

@endsection
