@extends('layouts.app')

@section('css')
	<link rel="stylesheet" type="text/css" href="./css/category.css">
@endsection

@section('content')
	<div class="container col-12">
		<div class="row">
			<div class="col-12">		
				{{-- same as if admin == 1--}}
				@can('isAdmin')
					<h2 class="text-center">Categories</h2>
					<div class="text-center d-flex justify-content-center">
						<a href="/categories/create" class="text-center"><i class="fas fa-plus " style="color:black;"></i> <span style="color:black;">Add New Products</span></a>
					</div>
					<br>
					

					<table class="table col-8 offset-2 text-center ">
					<thead class="thead-light text-center">
						<tr>
							<th>Category ID</th>
							{{-- <th><input id="#selectAll" type="checkbox" aria-label="Checkbox for following text input"></th>
							<th></th> --}}
							<th>Category Name</th>
								
							<th>Status</th>
							
							<th>Actions</th>
						</tr>
					</thead>

					<tbody>
						@foreach($categories as $category)
							<tr>
								<td>C{{$category->id}}</td>
								{{-- <td><input id="#selectAll" type="checkbox" aria-label="Checkbox for following text input"></td>
								<td><div style="width: 25%;" class="mx-auto ">
									<img src="{{asset($category->imgPath)}}" class="card-img-top">
								</div></td>  --}}
								<td><a href="/categories/{{$category->id}}">{{$category->name}}</a></td>

					
								
								<td>
									@if($category->isActive ==1)
										{{"Active"}}
									@else
										{{"Inactive"}}
									@endif
						
								</td>
							
								<td>
									
									<form method="POST" action="/categories/{{$category->id}}">
										@csrf
										@method('DELETE')
										@if($category->isActive == 1)
											<button type="submit"  class="btn btn-outline-danger" style="float:right;">Deactivate</button>
										@else
											<button type="submit" class="btn btn-outline-success" style="float:right;">Reactivate</button>
										@endif
									</form>
									<a href="/categories/{{$category->id}}/edit" class="btn btn-outline-secondary" style="float:right;">Edit</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				@else
					{{-- check if a session flash cariable containing a notification message is set --}}
					@if(session('status'))
						<span>{{session('status')}}</span>
					@endif
					{{-- catalogue view for non-admin users --}}

					<div id="carouselExampleControls" class="carousel slide mb-5" data-ride="carousel">
					  <div class="carousel-inner">

					    <div class="carousel-item active">
					      <img src="./images/carousel1.jpg" class="d-block w-100" alt="" >
					    </div>

					    <div class="carousel-item">
					      <img src="./images/carousel2.jpg" class="d-block w-100" alt="...">
					    </div>

					    <div class="carousel-item">
					      <img src="./images/carousel3.jpg" class="d-block w-100" alt="...">
					    </div>

					  </div>

					  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
					    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
					    <span class="sr-only">Previous</span>
					  </a>

					  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
					    <span class="carousel-control-next-icon" aria-hidden="true"></span>
					    <span class="sr-only">Next</span>
					  </a>

					</div>
					{{-- end of carousel --}}

					{{-- about --}}
					<div class="row">
						<div class="offset-2 col-8 text-center">
							<h2 style="font-weight: bold;">Workplace Closet</h2>
							<p>by ZARA</p>
							<p style="font-size: 20px;">Workplace Closet is an initiative by ZARA to give thanks to all of their 150,000 employees by allowing them to borrow and return clothes from their inhouse brand catalog.</p>
							<p style="font-size: 20px;">With the current production of over 450 Million clothing articles a year, ZARA envisions  sustainable and circular ecosystem--where everyone who is within their company is able to contribute their part simply by using and reusing the company's dead stock inventories, their trial sizes, and other reusable items.</p>
							<br>
							<br>
						</div>
						
					</div>



					<div class="row">
						@foreach($categories as $category)
							<div class= col-4 class="mb-5 px-1 d-flex catdiv">
								<div class="text-center">
									<a href="/categories/{{$category->id}}">
									<div class="d-flex justify-content-center align-self-center" style=" margin-bottom: -100px; background-color: white; height:100px; width:180px; float:left;" class="box">
										<h4 class="d-flex align-self-center" style="color: black; text-align: center;">{{$category->name}}</h4>
									</div>
									</a>

									
										<img src="{{asset($category->imgPath)}}" class="img-fluid d-flex catsize" style="position:relative; z-index:-2; float:left; margin-bottom: 50px;">
									
								
								</div>
							</div>




							{{-- <div class="col-3 p-4">
								<div style="width: 50%;" class="mx-auto">
									<img src="{{asset($category->imgPath)}}" class="img-fluid card-img-top" width="100%">
								</div>
								<div class="card-body">
									<a href="/categories/{{$category->id}}"><h4 class="card-title">{{$category->name}}</h4></a>
									<p class="card-text">{{$category->description}}</p>
									<p class="card-text">{{$category->price}}</p>
									
								</div>
							</div> --}}
				
						@endforeach
					</div>
				@endcan
				</div>
			</div>
		</div>
	</div>

@endsection