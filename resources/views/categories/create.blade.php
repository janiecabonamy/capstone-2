@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">

			<div class="col-12">
				<div class="row">
					<div class="col-6">
						

						<div>
							@if ($errors->any())
						         <div class="alert alert-danger">
				  		         <div>
				  		         	<button type="button" class="close" data-dismiss="alert">&times;</button>
				  		         </div>
						            <ul>
						                @foreach ($errors->all() as $error)
						                    <li>{{ $error }}</li>
						                @endforeach
						            </ul>
				  		         </div>	     
							@endif
						</div>
						{{-- end of error check --}}

						<h2 class="text-align"style= "font-family: 'Montserrat', sans-serif; font-weight: 300; text-transform: uppercase;">Add Categories</h2>
						<br>
						
						{{-- never forget to use enctype="multipart/form-data" for file uploads --}}
						<form method="POST" action="/categories" enctype="multipart/form-data">
							@csrf
							<div class="form-group">
								<label for="name">Category name: </label>
								<input class="form-control" type="text" id="catName" name="catName">

								<label for="name">Category code: </label>
								<input class="form-control" type="text" id="catCode" name="catCode">

								<label for="image">Image:</label>
								<input class="form-control" type="file" name="catImage" id="catImage">

							</div>
							<button type="submit" class="btn btn-outline-success" id="addCatBtn">Add Category</button>
						</form>

						<br>
					</div>
					<div class="col-6">
						<h2 class="text-align" style= "font-family: 'Montserrat', sans-serif; font-weight: 300; text-transform: uppercase;">Add Products</h2>
						<br>
						<form method="POST" action="/assets" enctype="multipart/form-data">
							@csrf

							<div class="form-group">
								<label for="category_id">Category: </label>
								<select class="form-control" id="category_id" name="category">
									<option>Select a category:</option>
									@if(count($categories) > 0)
										@foreach($categories as $category)
											<option value="{{$category->id}}">{{$category->name}}</option>
										@endforeach
									@endif
								</select>
							</div>

							<div class="form-group">
								<label for="name">Product Title: </label>
								<input class="form-control" type="text" name="prodTitle" id="prodTitle">
							</div>
							<div class="form-group">
								<label for="description">Description: </label>
								<input class="form-control" type="text" name="prodDesc" id="prodDesc">
							</div>
							
							<label for="prodSize">Quantity per Size: </label>
								<table id="example" class="display" style="width:80%">
							        <thead>
							            <tr class="justify-content-center">
							                <th>Free</th>
							                <th>XS</th>
							                <th>S</th>
							            </tr>
							        </thead>
							        <tbody>
							            <tr>
							               <td><input type="number" min="0" id="freeQty" name="freeQty" ></td>
							               <td><input type="number" min="0" id="xsQty" name="xsQty" ></td>
							               <td><input type="number" min="0" id="sQty" name="sQty" ></td>
							           </tr>
							        </tbody>

							        <thead>
							            <tr class="justify-content-center">
							                <th>M</th>
							                <th>L</th>
							                <th>XL</th>
							            </tr>
							        </thead>
							        <tbody>
							            <tr>
							               <td><input type="number" min="0" id="mQty" name="mQty" ></td>
							               <td><input type="number" min="0" id="lQty" name="lQty" ></td>
							               <td><input type="number" min="0" id="xlQty" name="xlQty" ></td>
							           </tr>
							        </tbody>
							    </table>
							
							<div class="form-group">
								<label for="image">Image:</label>
								<input class="form-control" type="file" name="prodImage" id="prodImage">
							</div>

							<button type="submit" class="btn btn-outline-success">
								Add Product
							</button>							
						</form>
					</div>
				</div>
				<p></p>
			</div>
		</div>
	</div>
@endsection