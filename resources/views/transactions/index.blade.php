@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="text-center">Reservations</h2>
				<br>
				<br>
				
				{{-- REQUESTED TABLE --}}
				<h6 class="text-center">Pending Reservations</h6>
				<br>
				<table class="table">
					<thead class="thead-light text-center">
						<tr>
							<th>Reservation number</th>
							@can('isAdmin')
							<th>User</th>
							@endcan
							<th>Product Name</th>
							<th>Description</th>
							<th>Size</th>
							<th>Category</th>
							<th>Borrow Date</th>
							<th>Return Date</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>

					<tbody>
							@foreach($transactions as $transaction)
							@foreach($categories as $category)
							@foreach($assets as $asset)
								@if($category->id == $asset->category_id && $transaction->asset_id == $asset->id && $transaction->status_id == 1)
									@can('isAdmin')
										<tr>
											<td>{{$transaction->id}}</td>
											<td>{{$transaction->user->name}}</td>

											<td><a href="/transactions/{{$asset->id}}">{{$asset->name}}</a></td>
											<td>{{$asset->description}}</td>
											<td>{{$transaction->size}}</td>
											<td>{{$asset->category->name}}</td>
											<td>{{$transaction->borrowDate}}</td>
											<td>{{$transaction->returnDate}}</td>
											<td>{{$transaction->status->name}}</td>
											<td>
												<form method="POST" action="/transactions/{{$transaction->id}}" >
													@csrf
													@method('PUT')
													<input type="" name="status" value=4 hidden>
													<input type="" name="view" value="index" hidden>
													<button class="btn btn-outline-success" style="float:right;">Approve</button>
												</form>

												<form method="POST" action="/transactions/{{$transaction->id}}" >
													@csrf
													@method('PUT')
													<input type="" name="status" value=3 hidden>
													<input type="" name="view" value="index" hidden>
													<button class="btn btn-outline-danger" style="float:right;">Reject</button>
												</form>
											</td>
										</tr>
									@else

									@if($transaction->user_id ==Auth::user()->id)
										<tr>
											<td>{{$transaction->id}}</td>
											{{-- <td>{{$transaction->user->name}}</td> --}}
											<td><a href="/transactions/{{$asset->id}}">{{$asset->name}}</a></td>
											<td>{{$asset->description}}</td>			
											<td>{{$transaction->size}}</td>
											<td>{{$asset->category->name}}</td>
											<td>{{$transaction->borrowDate}}</td>
											<td>{{$transaction->returnDate}}</td>
											<td>{{$transaction->status->name}}</td>
											<td>	
												<form method="POST" action="/transactions/{{$transaction->id}}" >
													@csrf
													@method('PUT')
													<input type="" name="status" value=2 hidden>
													<input type="" name="view" value="index" hidden>
													<button class="btn btn-outline-danger" style="float:right;">Cancel</button>
												</form>
											</td>
										
										</tr>
									@endif
									@endcan
								@endif
							@endforeach	
							@endforeach
							@endforeach
					</tbody>
				</table>

				{{-- APPROVED TABLE --}}
				<h6 class="text-center">Approved Reservations</h6>
				<br>
				<table class="table">
					<thead class="thead-light text-center">
						<tr>
							<th>Reservation number</th>
							@can('isAdmin')
							<th>User</th>
							@endcan
							<th>Product Name</th>
							<th>Description</th>
							<th>Size</th>
							<th>Category</th>
							<th>Borrow Date</th>
							<th>Return Date</th>
							<th>Asset Assigned</th>
							<th>Status</th>
							@can('isAdmin')
							<th>Action</th>
							@endcan
						</tr>
					</thead>

					<tbody>
							@foreach($transactions as $transaction)
							@foreach($categories as $category)
							@foreach($assets as $asset)
								@if($category->id == $asset->category_id && $transaction->asset_id == $asset->id && $transaction->status_id == 4)
									@can('isAdmin')
										<tr>
											<td>{{$transaction->id}}</td>
											<td>{{$transaction->user->name}}</td>
											<td>{{$asset->name}}</td>
											<td>{{$asset->description}}</td>
											<td>{{$transaction->size}}</td>
											<td>{{$asset->category->name}}</td>
											<td>{{$transaction->borrowDate}}</td>
											<td>{{$transaction->returnDate}}</td>
											<td>{{$transaction->assigned}}</td>
											<td>{{$transaction->status->name}}</td>
											<td>
												<form method="POST" action="/transactions/{{$transaction->id}}" >
													@csrf
													@method('PUT')
													<input type="" name="status" value=5 hidden>
													<input type="" name="view" value="index" hidden>
													<button class="btn btn-outline-success" style="float:right;">Returned</button>
												</form>
											</td>
										</tr>
									@else

									@if($transaction->user_id ==Auth::user()->id)
										<tr>
											<td>{{$transaction->id}}</td>
											{{-- <td>{{$transaction->user->name}}</td> --}}
											<td>{{$asset->name}}</td>
											<td>{{$asset->description}}</td>			
											<td>{{$transaction->size}}</td>
											<td>{{$asset->category->name}}</td>
											<td>{{$transaction->borrowDate}}</td>
											<td>{{$transaction->returnDate}}</td>
											<td>{{$transaction->assigned}}</td>
											<td>{{$transaction->status->name}}</td>
											
										</tr>
									@endif
									@endcan
								@endif
							@endforeach	
							@endforeach
							@endforeach
					</tbody>
				</table>

				{{-- COMPLETED TABLE --}}
				<h6 class="text-center">Completed Transactions</h6>
				<br>
				<table class="table mb-5">
					<thead class="thead-light text-center">
						<tr>
							<th>Reservation number</th>
							@can('isAdmin')
							<th>User</th>
							@endcan
							<th>Product Name</th>
							<th>Description</th>
							<th>Size</th>
							<th>Category</th>
							<th>Borrow Date</th>
							<th>Return Date</th>
							<th>Status</th>
						</tr>
					</thead>

					<tbody>
							@foreach($transactions as $transaction)
							@foreach($categories as $category)
							@foreach($assets as $asset)
								@if($category->id == $asset->category_id && $transaction->asset_id == $asset->id)
								@if($transaction->status_id == 2 || $transaction->status_id == 3 || $transaction->status_id == 5)
									@can('isAdmin')
										<tr>
											<td>{{$transaction->id}}</td>
											<td>{{$transaction->user->name}}</td>
											<td>{{$asset->name}}</td>
											<td>{{$asset->description}}</td>
											<td>{{$transaction->size}}</td>
											<td>{{$asset->category->name}}</td>
											<td>{{$transaction->borrowDate}}</td>
											<td>{{$transaction->returnDate}}</td>
											<td>{{$transaction->status->name}}</td>
										</tr>
									@else

									@if($transaction->user_id ==Auth::user()->id )
										<tr>
											<td>{{$transaction->id}}</td>
											{{-- <td>{{$transaction->user->name}}</td> --}}
											<td>{{$asset->name}}</td>
											<td>{{$asset->description}}</td>			
											<td>{{$transaction->size}}</td>
											<td>{{$asset->category->name}}</td>
											<td>{{$transaction->borrowDate}}</td>
											<td>{{$transaction->returnDate}}</td>
											<td>{{$transaction->status->name}}</td>
										</tr>
									@endif
									@endcan
								@endif
								@endif
							@endforeach	
							@endforeach
							@endforeach
					</tbody>
				</table>

			</div>
		</div>
	</div>
@endsection