@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12">
				@can('isAdmin')
					<h2 class="text-center">All Products</h2>
					<div class="text-center d-flex justify-content-center">
						<a href="/categories/create" class="text-center"><i class="fas fa-plus " style="color:black;"></i> <span style="color:black;">Add New Products</span></a>
					</div>
					<br>

					<table class="table">
					<thead class="thead-light text-center">
						<tr>
							<th>Asset ID</th>
							<th>Product</th>
							<th>Product Name</th>
							<th>Description</th>
							<th>Freesize</th>
							<th>Extra Small</th>
							<th>Small</th>
							<th>Medium</th>
							<th>Large</th>
							<th>Extra Large</th>
							<th>Status</th>
							<th>Category</th>
							<th>Actions</th>
						</tr>
					</thead>

					<tbody>
						@foreach($assets as $asset)
							
							<tr>
								<td>C{{$asset->category->id}}-A{{$asset->id}}</td>
								<td><div style="width: 100%; height:100%;" class="">
									<img src="{{asset($asset->imgPath)}}" class="card-img-top">
								</div></td> 

								<td><a href="/assets/{{$asset->id}}">{{$asset->name}}</a></td>
								<td style="text-overflow:ellipsis; overflow:hidden;">{{$asset->description}}</td>
								@if($asset->isActive ==1)
									<td>{{$freesize[$countA++]}}</td>
									<td>{{$extrasmall[$countB++]}}</td>
									<td>{{$small[$countC++]}}</td>
									<td>{{$medium[$countD++]}}</td>
									<td>{{$large[$countE++]}}</td>
									<td>{{$extralarge[$countF++]}}</td>
								@else
									<td class="text-center" colspan="6">{{"Asset is not available"}}</td>
								@endif
								<td>
									@if($asset->isActive ==1)
										{{"Active"}}
									@else
										{{"Inactive"}}
									@endif
								</td>
								<td>{{$asset->category->name}}</td>
								<td>
									<a href="/assets/{{$asset->id}}/edit" class="btn btn-outline-secondary" style="float:right;">Edit</a>
									<form method="POST" action="/assets/{{$asset->id}}">
										@csrf
										@method('DELETE')
										@if($asset->isActive == 1)
											<button type="submit"  class="btn btn-outline-danger" style="float:right;">Deactivate</button>
										@else
											<button type="submit" class="btn btn-outline-success" style="float:right;">Reactivate</button>
										@endif
									</form>
								</td>
							</tr>
							
						@endforeach
					</tbody>
				@else
				
					{{-- check if a session flash cariable containing a notification message is set --}}
					@if(session('status'))
						<span>{{session('status')}}</span>
					@endif
					{{-- catalogue vie for non-admin users --}}


					<div class="row">
						@foreach($assets as $asset)
							@if($asset->isActive == 1)
								<div class="col-sm-6 col-lg-3 p-4">
									<div style="width: 100%;" class="mx-auto">
										<img src="{{asset($asset->imgPath)}}" class="card-img-top" width="100%">
									</div>
									<div class="card-body">
										<h4 class="card-title">{{$asset->name}}</h4>
										<p class="card-text">{{$asset->description}}</p>
										<p class="card-text">{{$asset->price}}</p>
									</div>
									<div class="card-footer text-muted">
										

										{{-- <form method="POST" action="/transactions/{{$asset->id}}" enctype="multipart/form-data" > --}}
										<form method="POST" action="/transactions" >
										@csrf
										<input name="userid" value="{{Auth::user()->id}}" hidden="">
										<input name="assetid" value="{{$asset->id}}" hidden="">
										<div class="form-group">
											<label for="size">Size </label>
											<select id="size" name="size">


												<option>Available Sizes</option>
												@if(count($freeavail[$asset->id]) > 0)
													<option value="freesize">freesize</option>
												@endif

												@if(count($xsmallavail[$asset->id]) > 0)
													<option value="extra small">extra small</option>
												@endif

												@if(count($smallavail[$asset->id]) > 0)
													<option value="small">small</option>
												@endif

												@if(count($mediumavail[$asset->id]) > 0)
													<option value="medium">medium</option>
												@endif

												@if(count($largeavail[$asset->id]) > 0)
													<option value="large">large</option>
												@endif

												@if(count($xlargeavail[$asset->id]) > 0)
													<option value="extra large">extra large</option>
												@endif
								
											</select>
										</div>
										<div class="form-group">
										<label>Borrow date:</label>
										<input type="date" name="borrowdate" >
										<label>Return date:</label>
										<input type="date" name="returndate">
										</div>
										<button type="submit" class="reserve">Reserve</button>
									</form>
									</div>
								</div>
							@endif
						@endforeach
					</div>
				@endcan
				


			</div>

		</div>
		
	</div>

@endsection