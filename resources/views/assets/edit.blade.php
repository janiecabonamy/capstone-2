@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-8 offset-2">
			<div class="card">
				<div class="card-header">
					Edit Product
				</div>
				<div class="card-body">
					{{-- error checker --}}
					
					<div>
						@if ($errors->any())
							         <div class="alert alert-danger">
					  		         <div>
					  		         	<button type="button" class="close" data-dismiss="alert">&times;</button>
					  		         </div>
								            <ul>
								                @foreach ($errors->all() as $error)
								                    <li>{{ $error }}</li>
								                @endforeach
								            </ul>
					  		         </div>
								     
						@endif
					</div>
					{{-- end of error check --}}
					<form method="POST" action="/assets/{{$asset->id}}" enctype="multipart/form-data">
						@csrf
						@method('PUT')
						<div class="form-group">
							<label for="name">Name: </label>
							<input class="form-control" type="text" name="name" id="name" value="{{$asset->name}}">
						</div>
						<div class="form-group">
							<label for="description">Description: </label>
							<input class="form-control" type="text" name="description" id="description" value="{{$asset->description}}">
						</div>
						
						<div class="form-group">
							<label for="category_id">Category: </label>
							<select class="form-control" id="category_id" name="category">
								<option>Select a category:</option>
								@if(count($categories) > 0)
									@foreach($categories as $category)
										@if($asset->category_id == $category->id)
											<option value="{{$category->id}}" selected>{{$category->name}}</option>
										@else
											<option value="{{$category->id}}">{{$category->name}}</option>
										@endif
									@endforeach
								@endif
							</select>
						</div>
						<div class="form-group">
							<label for="image">Image:</label>
							<input class="form-control" type="file" name="image" id="image">
						</div>

						<button type="submit" class="btn btn-success">
							Edit Product
						</button>							
					</form>
				</div>
			</div>
		</div>
	</div>

@endsection
