<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    {{-- <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> --}}
    <link href="https://fonts.googleapis.com/css?family=Bebas+Neue|Julius+Sans+One|Pontano+Sans&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700|Montserrat:300" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    {{-- common external css --}}
        <link rel="stylesheet" type="text/css" href="./css/all.css">

    {{-- font awesome link --}}
    <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">

    {{-- css bootstrap --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    {{-- for specific page external css --}}
    <main class="">
        @yield('css')
    </main>
</head>
<body>
    <div id="app">
        <div class="alert alert-light justify-content-center d-flex align-items-baseline" role="alert" style="height:10px;">
         This website was made for Skills Development and Demonstration purposes only. All images were taken from either<span class="text-white">-</span> <a href="www.google.com" class="alert-link"> Google </a><span class="text-white">-</span> or the<span class="text-white">-</span> <a href="www.zara.com.ph" class="alert-link"> Official ZARA Philippines </a><span class="text-white">-</span>website. 
        </div>

        <nav class="navbar navbar-expand-md navbar-dark shadow-sm sticky-top" style=" background-color: black; height:80px;font-family: 'Montserrat', sans-serif; font-weight: 300;">
            <div class="container mx-0 px-0 col-12 bg-black">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <!-- place brand name here (navbar)-->
                    {{ config('app.name', 'Laravel') }} 

                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav ml-auto pr-1">
                        <li class="nav-item pb-0"><a class="nav-link" href="/categories"><span style="color:white;">Collections</span></a></li>
                        <li class="nav-item pb-0"><a class="nav-link" href="/assets"><span style="color:white;">All Products</span></a></li>
                        <li class="nav-item pb-0"><a class="nav-link" href="/transactions"><span style="color:white;">Reservations</span></a>
                        
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <div class="col-12">
            <main class="py-4">
                @yield('content')
            </main>

        </div> 
        <footer>
        <div class="row fixed-bottom" style="background-color: black; height: 40px;">
            <div class="offset-1 col-10 text-center text-light mt-2">
                <p class="align-text-top">Thank you for all your hard work!</p>
                <br>
            </div>
        </div>
    </footer>
            
    </div>
           
        
</body>
</html>
