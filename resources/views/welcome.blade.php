<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Zara @ the Factory</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Bebas+Neue|Julius+Sans+One|Pontano+Sans&display=swap" rel="stylesheet">

        

        {{-- font-family: 'Bebas Neue', cursive;
        font-family: 'Pontano Sans', sans-serif;
        font-family: 'Julius Sans One', sans-serif; --}}

        <!-- Styles -->
        <style>
            *{
                /*border: black 1px solid;*/
            }

            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Julius Sans One', sans-serif;
                font-weight: 500;
                height: 100vh;
                margin: 0;

            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                margin-top:50px;
                font-size: 24px;
                margin-bottom: 50px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 300;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;

            }

            .m-b-md {
                margin-bottom: 30px;
            }

            img{
                width:100%;
                height: 100%;
                background-position: center;
                background-size: cover;
                background-repeat: no-repeat;
                padding: 10px;
            }
            .img-box{
                width:500px;
                height:125px;
                 
            }
        </style>
    </head>
    <body>

        <div class="flex-center position-ref" style="height:96vh;">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <a href="/home"><div class="img-box"><img src="./images/zaralogo.jpg" ></div></a>
                <div class="title m-b-md font-weight-bold">
                     WORKPLACE CLOSET
                </div>

                <div class="px-0 mx-0">
                    An exclusive clothes sharing portal for ALL ZARA EMPLOYEES
                </div>
            </div>
        </div>

        <div class="alert alert-light text-center" role="alert" style="height:30px;">
         <p style="text-transform:initial !important;">This website was made for Skills Development and Demonstration purposes only. All images were taken from either<span class="text-white">-</span> <a href="www.google.com" class="alert-link"> Google </a><span class="text-white">-</span> or the<span class="text-white">-</span> <a href="www.zara.com.ph" class="alert-link"> Official ZARA Philippines </a><span class="text-white">-</span>website.</p> 
        </div>
    </body>
</html>
