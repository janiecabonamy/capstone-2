<?php

use Illuminate\Database\Seeder;

class TransactionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('transactions')->delete();
        
        \DB::table('transactions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 2,
                'asset_id' => 17,
                'size' => 'freesize',
                'status_id' => 2,
                'borrowDate' => '2020-05-19 00:00:00',
                'returnDate' => '2020-05-21 00:00:00',
                'assigned' => NULL,
                'created_at' => '2020-05-05 00:25:07',
                'updated_at' => '2020-05-05 06:03:54',
            ),
            1 => 
            array (
                'id' => 2,
                'user_id' => 2,
                'asset_id' => 36,
                'size' => 'medium',
                'status_id' => 4,
                'borrowDate' => '2020-05-15 00:00:00',
                'returnDate' => '2020-05-17 00:00:00',
                'assigned' => 189,
                'created_at' => '2020-05-05 06:03:34',
                'updated_at' => '2020-05-05 06:52:01',
            ),
            2 => 
            array (
                'id' => 3,
                'user_id' => 2,
                'asset_id' => 44,
                'size' => 'freesize',
                'status_id' => 3,
                'borrowDate' => '2020-05-08 00:00:00',
                'returnDate' => '2020-05-09 00:00:00',
                'assigned' => NULL,
                'created_at' => '2020-05-05 06:55:35',
                'updated_at' => '2020-05-05 06:57:18',
            ),
            3 => 
            array (
                'id' => 4,
                'user_id' => 2,
                'asset_id' => 10,
                'size' => 'small',
                'status_id' => 5,
                'borrowDate' => '2020-05-13 00:00:00',
                'returnDate' => '2020-05-14 00:00:00',
                'assigned' => 51,
                'created_at' => '2020-05-05 06:56:35',
                'updated_at' => '2020-05-05 06:57:29',
            ),
            4 => 
            array (
                'id' => 5,
                'user_id' => 2,
                'asset_id' => 57,
                'size' => 'freesize',
                'status_id' => 1,
                'borrowDate' => '2020-05-09 00:00:00',
                'returnDate' => '2020-05-13 00:00:00',
                'assigned' => NULL,
                'created_at' => '2020-05-05 06:57:00',
                'updated_at' => '2020-05-05 06:57:00',
            ),
            5 => 
            array (
                'id' => 6,
                'user_id' => 2,
                'asset_id' => 46,
                'size' => 'small',
                'status_id' => 1,
                'borrowDate' => '2020-05-26 00:00:00',
                'returnDate' => '2020-05-27 00:00:00',
                'assigned' => NULL,
                'created_at' => '2020-05-05 06:57:55',
                'updated_at' => '2020-05-05 06:57:55',
            ),
        ));
        
        
    }
}