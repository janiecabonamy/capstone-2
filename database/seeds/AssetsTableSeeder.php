<?php

use Illuminate\Database\Seeder;

class AssetsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('assets')->delete();
        
        \DB::table('assets')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'RUFFLED POPLIN TOP',
                'description' => 'Top with a plunging neckline, wide ruffled straps and an asymmetric hem.',
                'imgPath' => 'images/1588581965.jpg',
                'isActive' => 1,
                'category_id' => 1,
                'created_at' => '2020-05-04 08:46:05',
                'updated_at' => '2020-05-05 01:42:30',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'CROPPED CHECKED TOP',
                'description' => 'Crop top featuring a loose-fitting neckline and balloon sleeves falling below the elbow with ruffled trim. Elastic draped details.',
                'imgPath' => 'images/1588582450.jpg',
                'isActive' => 1,
                'category_id' => 1,
                'created_at' => '2020-05-04 08:54:10',
                'updated_at' => '2020-05-05 01:42:30',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'RUFFLED BLOUSE',
                'description' => 'Semi-sheer blouse featuring a round neckline with adjustable ties. Short full sleeves. Ruffled hem.',
                'imgPath' => 'images/1588582515.jpg',
                'isActive' => 1,
                'category_id' => 1,
                'created_at' => '2020-05-04 08:55:15',
                'updated_at' => '2020-05-05 01:42:30',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'TOP WITH CUTWORK EMBROIDERY',
                'description' => 'Top with a wide neckline, sleeves falling below the elbow with elastic trims. Featuring cutwork embroidery and ruffles.',
                'imgPath' => 'images/1588586853.jpg',
                'isActive' => 1,
                'category_id' => 1,
                'created_at' => '2020-05-04 08:55:53',
                'updated_at' => '2020-05-05 01:42:30',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'STRAPPY TOP',
                'description' => 'Top featuring a straight neckline and thin straps. Matching trousers available.',
                'imgPath' => 'images/1588582679.jpg',
                'isActive' => 1,
                'category_id' => 1,
                'created_at' => '2020-05-04 08:57:59',
                'updated_at' => '2020-05-05 01:42:30',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'BALLOON SLEEVE SHIRT',
                'description' => 'Cropped shirt with wide lapels, short puff sleeves and a ruffled hem. Button-up front.',
                'imgPath' => 'images/1588582762.jpg',
                'isActive' => 1,
                'category_id' => 1,
                'created_at' => '2020-05-04 08:59:22',
                'updated_at' => '2020-05-05 01:42:30',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'PRINTED TOP',
                'description' => 'V-neck crop top featuring short puff sleeves, elastic cuffs, tie detail at the front and smocking at the back.',
                'imgPath' => 'images/1588582813.jpg',
                'isActive' => 1,
                'category_id' => 1,
                'created_at' => '2020-05-04 09:00:13',
                'updated_at' => '2020-05-05 01:42:30',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'FLORAL PRINT TOP',
                'description' => 'V-neck top with short puff sleeves. Featuring smocked fabric at the bottom and faux pearl buttons on the front.',
                'imgPath' => 'images/1588582876.jpg',
                'isActive' => 1,
                'category_id' => 1,
                'created_at' => '2020-05-04 09:01:16',
                'updated_at' => '2020-05-05 01:42:30',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'GEOMETRIC PRINT SHIRT',
                'description' => 'Cropped shirt featuring a lapel collar and V-neck, long sleeves, elastic trims and button-up front.',
                'imgPath' => 'images/1588582939.jpg',
                'isActive' => 1,
                'category_id' => 1,
                'created_at' => '2020-05-04 09:02:19',
                'updated_at' => '2020-05-05 01:42:30',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'TEXTURED WEAVE TOP',
                'description' => 'V-neck top featuring long sleeves with ruffled elastic trim. Matching textured fabric detail. Elastic trim at the waist.',
                'imgPath' => 'images/1588582987.jpg',
                'isActive' => 1,
                'category_id' => 1,
                'created_at' => '2020-05-04 09:03:07',
                'updated_at' => '2020-05-05 01:42:30',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'PRINTED POPLIN TOP',
                'description' => 'Top featuring an asymmetric neckline, one sleeve falling below the elbow with gathered detail. Ruffle trims. Invisible side zip fastening.',
                'imgPath' => 'images/1588583047.jpg',
                'isActive' => 1,
                'category_id' => 1,
                'created_at' => '2020-05-04 09:04:07',
                'updated_at' => '2020-05-05 01:42:30',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'CONTRAST TOP',
                'description' => 'Semi-sheer top featuring a round neckline, short puff sleeves with elastic trim and matching appliqué.',
                'imgPath' => 'images/1588583177.jpg',
                'isActive' => 1,
                'category_id' => 1,
                'created_at' => '2020-05-04 09:06:17',
                'updated_at' => '2020-05-05 01:42:30',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'TIE-NECK TOP',
                'description' => 'Sleeveless top with a high neck. Elastic hem. Buttoned opening at the back and tie detail.',
                'imgPath' => 'images/1588583248.jpg',
                'isActive' => 1,
                'category_id' => 1,
                'created_at' => '2020-05-04 09:07:28',
                'updated_at' => '2020-05-05 01:42:30',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'BLOUSE WITH COMBINED EMBROIDERY',
                'description' => 'Blouse with an asymmetric collar and short sleeves with ruffled cuffs. Contrast embroidered motifs. Button-up front.',
                'imgPath' => 'images/1588583418.jpg',
                'isActive' => 1,
                'category_id' => 1,
                'created_at' => '2020-05-04 09:10:18',
                'updated_at' => '2020-05-05 01:42:30',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'FULL POPLIN TOP',
                'description' => 'Sleeveless V-neck top with pleated ruffle trims. Button fastening on the back.',
                'imgPath' => 'images/1588583454.jpg',
                'isActive' => 1,
                'category_id' => 1,
                'created_at' => '2020-05-04 09:10:54',
                'updated_at' => '2020-05-05 01:42:30',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'TEXTURED WEAVE KNIT CARDIGAN',
                'description' => 'Knit collared cardigan with long sleeves and front button fastening.',
                'imgPath' => 'images/1588584032.jpg',
                'isActive' => 1,
                'category_id' => 2,
                'created_at' => '2020-05-04 09:20:32',
                'updated_at' => '2020-05-04 22:06:17',
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'KNIT VEST WITH CROCHET DETAIL',
                'description' => 'Knit V-neck vest with ribbed trims and matching crochet detail.',
                'imgPath' => 'images/1588584081.jpg',
                'isActive' => 1,
                'category_id' => 2,
                'created_at' => '2020-05-04 09:21:21',
                'updated_at' => '2020-05-04 22:06:17',
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'POINTELLE CROP TOP',
                'description' => 'Sleeveless knit top with a round neckline and ruffled armholes. Button fastening on the front.',
                'imgPath' => 'images/1588584133.jpg',
                'isActive' => 1,
                'category_id' => 2,
                'created_at' => '2020-05-04 09:22:14',
                'updated_at' => '2020-05-04 22:06:17',
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'CROPPED EMBROIDERED TOP',
                'description' => 'Knit top with a wide neckline and short puff sleeves. Featuring matching embroidered fabric.',
                'imgPath' => 'images/1588584178.jpg',
                'isActive' => 1,
                'category_id' => 2,
                'created_at' => '2020-05-04 09:22:58',
                'updated_at' => '2020-05-04 22:06:17',
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'CONTRASTING KNIT CARDIGAN',
                'description' => 'Knit cardigan featuring a round neck, long sleeves puffed at the shoulders, matching fabric appliqué detail and button-up front.',
                'imgPath' => 'images/1588584238.jpg',
                'isActive' => 1,
                'category_id' => 2,
                'created_at' => '2020-05-04 09:23:58',
                'updated_at' => '2020-05-04 22:06:17',
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'PINK BUTTONED CARDIGAN',
                'description' => 'Knit cardigan with a round neckline and long sleeves. Button-up front.',
                'imgPath' => 'images/1588584294.jpg',
                'isActive' => 1,
                'category_id' => 2,
                'created_at' => '2020-05-04 09:24:54',
                'updated_at' => '2020-05-04 22:06:17',
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'GREEN BUTTONED CARDIGAN',
                'description' => 'Knit cardigan with a round neckline and long sleeves. Button-up front.',
                'imgPath' => 'images/1588584337.jpg',
                'isActive' => 1,
                'category_id' => 2,
                'created_at' => '2020-05-04 09:25:37',
                'updated_at' => '2020-05-04 22:36:11',
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'BASIC SWEATER',
                'description' => 'Long sleeve knit sweater with a round neckline. Featuring side vents at the hem and ribbed trims.',
                'imgPath' => 'images/1588584378.jpg',
                'isActive' => 1,
                'category_id' => 2,
                'created_at' => '2020-05-04 09:26:18',
                'updated_at' => '2020-05-04 22:06:17',
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'RIBBED KNIT POLO SHIRT',
                'description' => 'Collared knit polo shirt with long sleeves and front button fastening.',
                'imgPath' => 'images/1588584427.jpg',
                'isActive' => 1,
                'category_id' => 2,
                'created_at' => '2020-05-04 09:27:07',
                'updated_at' => '2020-05-04 22:06:17',
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'STRIPED KNIT TOP',
                'description' => 'Knit top with round neckline and short sleeves.',
                'imgPath' => 'images/1588584475.jpg',
                'isActive' => 1,
                'category_id' => 2,
                'created_at' => '2020-05-04 09:27:55',
                'updated_at' => '2020-05-04 22:06:17',
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'KNIT POLKA DOT TOP',
                'description' => 'Short top featuring a round neckline, sleeves falling below the elbow and ribbed trims.',
                'imgPath' => 'images/1588584534.jpg',
                'isActive' => 1,
                'category_id' => 2,
                'created_at' => '2020-05-04 09:28:54',
                'updated_at' => '2020-05-04 22:06:17',
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'CROPPED WOOL BLEND CARDIGAN',
                'description' => 'Knit cardigan made of a wool blend. Featuring a V-neckline, long sleeves and front button fastening.',
                'imgPath' => 'images/1588584576.jpg',
                'isActive' => 1,
                'category_id' => 2,
                'created_at' => '2020-05-04 09:29:36',
                'updated_at' => '2020-05-04 22:06:17',
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'KNIT CARDIGAN WITH PUFF SLEEVES',
                'description' => 'V-neck cardigan with short puff sleeves.',
                'imgPath' => 'images/1588584615.jpg',
                'isActive' => 1,
                'category_id' => 2,
                'created_at' => '2020-05-04 09:30:15',
                'updated_at' => '2020-05-04 22:06:17',
            ),
            28 => 
            array (
                'id' => 29,
                'name' => 'HI-RISE FULL LENGTH JEANS',
                'description' => 'Hi-rise wide-leg jeans featuring a five-pocket design. Zip and metal top button fastening.',
                'imgPath' => 'images/1588585842.jpg',
                'isActive' => 1,
                'category_id' => 3,
                'created_at' => '2020-05-04 09:50:42',
                'updated_at' => '2020-05-04 09:50:42',
            ),
            29 => 
            array (
                'id' => 30,
                'name' => 'EXTRA LONG WIDE-LEG PREMIUM CAIA ZW JEANS IN BLUE',
                'description' => 'Faded high-waist jeans featuring a five-pocket design, a wide-leg fit, front pleated detail and zip fly and metal top button fastening.',
                'imgPath' => 'images/1588586064.jpg',
                'isActive' => 1,
                'category_id' => 3,
                'created_at' => '2020-05-04 09:54:24',
                'updated_at' => '2020-05-04 09:54:24',
            ),
            30 => 
            array (
                'id' => 31,
                'name' => 'EXTRA LONG WIDE-LEG PREMIUM CAIA ZW JEANS IN WHITE',
                'description' => 'High-waist jeans with wide legs and pleated detail at the front, a five-pocket design, seamless hems and a zip fly and metallic top button fastening.',
                'imgPath' => 'images/1588586173.jpg',
                'isActive' => 1,
                'category_id' => 3,
                'created_at' => '2020-05-04 09:56:13',
                'updated_at' => '2020-05-04 09:56:13',
            ),
            31 => 
            array (
                'id' => 32,
                'name' => 'ZW PREMIUM ANKLE STRAIGHT TRUE BLUE JEANS',
                'description' => 'Faded high-waist jeans featuring a five-pocket design, frayed hems and front fastening with metal button.',
                'imgPath' => 'images/1588587110.jpg',
                'isActive' => 1,
                'category_id' => 3,
                'created_at' => '2020-05-04 10:11:50',
                'updated_at' => '2020-05-04 10:11:50',
            ),
            32 => 
            array (
                'id' => 33,
                'name' => 'ZW PREMIUM ‘80S MADISON BLUE JEANS',
                'description' => 'Faded-effect high-waist jeans. Featuring a five-pocket design, ripped detailing, a zip fly and a metal top button fastening.',
                'imgPath' => 'images/1588587202.jpg',
                'isActive' => 1,
                'category_id' => 3,
                'created_at' => '2020-05-04 10:13:22',
                'updated_at' => '2020-05-04 10:13:22',
            ),
            33 => 
            array (
                'id' => 34,
                'name' => 'JEANS ZW PREMIUM 80S HIGH WAIST FIERCE INK BLUE',
                'description' => 'Faded high waist skinny jeans with a five-pocket design and zip fly and metal top button fastening.',
                'imgPath' => 'images/1588587272.jpg',
                'isActive' => 1,
                'category_id' => 3,
                'created_at' => '2020-05-04 10:14:32',
                'updated_at' => '2020-05-04 10:14:32',
            ),
            34 => 
            array (
                'id' => 35,
                'name' => 'ZW PREMIUM GRAHAMS BLUE CULOTTE JEANS',
                'description' => 'High-waist jeans with a five-pocket design, seamless hems and a zip fly and metal top button fastening.',
                'imgPath' => 'images/1588587338.jpg',
                'isActive' => 1,
                'category_id' => 3,
                'created_at' => '2020-05-04 10:15:38',
                'updated_at' => '2020-05-04 10:15:38',
            ),
            35 => 
            array (
                'id' => 36,
                'name' => 'ALABAMA BLUE ZW PREMIUM KICK JEANS',
                'description' => 'Faded high-waist jeans with a five-pocket design, straight-leg design, seamless hems and a zip fly and metal top button fastening.',
                'imgPath' => 'images/1588587627.jpg',
                'isActive' => 1,
                'category_id' => 3,
                'created_at' => '2020-05-04 10:20:27',
                'updated_at' => '2020-05-04 10:20:27',
            ),
            36 => 
            array (
                'id' => 37,
                'name' => 'ZW PREMIUM FLORIDA BLUE JEANS',
                'description' => 'Faded high-waist jeans featuring a five-pocket design, ripped detail on the front, frayed hems, a zip fly and a metal top button fastening.',
                'imgPath' => 'images/1588587707.jpg',
                'isActive' => 1,
                'category_id' => 3,
                'created_at' => '2020-05-04 10:21:47',
                'updated_at' => '2020-05-04 10:21:47',
            ),
            37 => 
            array (
                'id' => 38,
                'name' => 'WHITE CULOTTE ZW PREMIUM JEANS',
                'description' => 'High-waist jeans with a five-pocket design, seamless hems and a zip fly and metal top button fastening.',
                'imgPath' => 'images/1588587807.jpg',
                'isActive' => 1,
                'category_id' => 3,
                'created_at' => '2020-05-04 10:23:27',
                'updated_at' => '2020-05-04 10:23:27',
            ),
            38 => 
            array (
                'id' => 39,
                'name' => 'ZW PREMIUM BOOTCUT CROPPED VINTAGE BLUE JEANS',
                'description' => 'Fitted high-waist jeans with a five-pocket design. Featuring a faded effect, ripped detailing on the front, frayed hems and zip fly with a metal top button fastening.',
                'imgPath' => 'images/1588587861.jpg',
                'isActive' => 1,
                'category_id' => 3,
                'created_at' => '2020-05-04 10:24:21',
                'updated_at' => '2020-05-04 10:24:21',
            ),
            39 => 
            array (
                'id' => 40,
                'name' => 'ZW PREMIUM NOKI MISTY BLUE JEANS',
                'description' => 'Faded effect high waist jeans with front pockets and back patch pockets. Zip fly and metal top button fastening.',
                'imgPath' => 'images/1588587938.jpg',
                'isActive' => 1,
                'category_id' => 3,
                'created_at' => '2020-05-04 10:25:38',
                'updated_at' => '2020-05-04 10:25:38',
            ),
            40 => 
            array (
                'id' => 41,
                'name' => 'JEANS ZW PREMIUM THE SAILOR STRAIGHT',
                'description' => 'High-waist jeans with front pockets. Featuring front darts, a wide-leg design, frayed hems and zip fly and metal top button fastening.',
                'imgPath' => 'images/1588588026.jpg',
                'isActive' => 1,
                'category_id' => 3,
                'created_at' => '2020-05-04 10:27:06',
                'updated_at' => '2020-05-04 10:27:06',
            ),
            41 => 
            array (
                'id' => 42,
                'name' => 'PLEATED SATIN FINISH SKIRT',
                'description' => 'Midi skirt with a stretch waistband.',
                'imgPath' => 'images/1588590199.jpg',
                'isActive' => 1,
                'category_id' => 4,
                'created_at' => '2020-05-04 11:03:19',
                'updated_at' => '2020-05-04 11:03:19',
            ),
            42 => 
            array (
                'id' => 43,
                'name' => 'PLEATED SKIRT',
                'description' => 'High-waist midi skirt with an elastic back.',
                'imgPath' => 'images/1588590307.jpg',
                'isActive' => 1,
                'category_id' => 4,
                'created_at' => '2020-05-04 11:05:07',
                'updated_at' => '2020-05-04 11:05:07',
            ),
            43 => 
            array (
                'id' => 44,
                'name' => 'POLKA DOT SKIRT',
                'description' => 'High-waist midi skirt with an elastic waistband and a front vent at the hem. Front button detail.',
                'imgPath' => 'images/1588590358.jpg',
                'isActive' => 1,
                'category_id' => 4,
                'created_at' => '2020-05-04 11:05:58',
                'updated_at' => '2020-05-04 11:05:58',
            ),
            44 => 
            array (
                'id' => 45,
                'name' => 'RUSTIC WRAP SKIRT WITH A BOW',
                'description' => 'High-waisted skirt featuring pleated detail at the front, a side fastening with interior buttons and a tied bow to one side.',
                'imgPath' => 'images/1588590448.jpg',
                'isActive' => 1,
                'category_id' => 4,
                'created_at' => '2020-05-04 11:07:28',
                'updated_at' => '2020-05-04 11:07:28',
            ),
            45 => 
            array (
                'id' => 46,
                'name' => 'VOLUMINOUS MIDI SKIRT',
                'description' => 'A-line skirt with invisible side zip fastening.',
                'imgPath' => 'images/1588590533.jpg',
                'isActive' => 1,
                'category_id' => 4,
                'created_at' => '2020-05-04 11:08:53',
                'updated_at' => '2020-05-04 11:08:53',
            ),
            46 => 
            array (
                'id' => 47,
                'name' => 'SKIRT WITH TULLE FRILL',
                'description' => 'Elastic high-waist skirt and matching ruffles.',
                'imgPath' => 'images/1588590582.jpg',
                'isActive' => 1,
                'category_id' => 4,
                'created_at' => '2020-05-04 11:09:42',
                'updated_at' => '2020-05-04 11:09:42',
            ),
            47 => 
            array (
                'id' => 48,
                'name' => 'ANIMAL PRINT SKIRT',
                'description' => 'High-waist midi skirt featuring an elastic waist and front vent at the hem.',
                'imgPath' => 'images/1588590643.jpg',
                'isActive' => 1,
                'category_id' => 4,
                'created_at' => '2020-05-04 11:10:43',
                'updated_at' => '2020-05-04 11:10:43',
            ),
            48 => 
            array (
                'id' => 49,
                'name' => 'SATIN SKIRT',
                'description' => 'High-waist midi skirt featuring an elastic waist and A-line fit.',
                'imgPath' => 'images/1588590718.jpg',
                'isActive' => 1,
                'category_id' => 4,
                'created_at' => '2020-05-04 11:11:58',
                'updated_at' => '2020-05-04 11:11:58',
            ),
            49 => 
            array (
                'id' => 50,
                'name' => 'ORIENTAL VOLUMINOUS MIDI SKIRT',
                'description' => 'High-waist skirt with an elastic waistband and seam detailing.',
                'imgPath' => 'images/1588590776.jpg',
                'isActive' => 1,
                'category_id' => 4,
                'created_at' => '2020-05-04 11:12:56',
                'updated_at' => '2020-05-04 11:12:56',
            ),
            50 => 
            array (
                'id' => 51,
                'name' => 'WRINKLED-EFFECT PRINTED SKIRT',
                'description' => 'High-waist midi skirt with an elastic waistband.',
                'imgPath' => 'images/1588590813.jpg',
                'isActive' => 1,
                'category_id' => 4,
                'created_at' => '2020-05-04 11:13:33',
                'updated_at' => '2020-05-04 11:13:33',
            ),
            51 => 
            array (
                'id' => 52,
                'name' => 'ECRU SATIN SKIRT',
                'description' => 'Flowing skirt with an elastic waistband.',
                'imgPath' => 'images/1588590865.jpg',
                'isActive' => 1,
                'category_id' => 4,
                'created_at' => '2020-05-04 11:14:25',
                'updated_at' => '2020-05-04 11:14:25',
            ),
            52 => 
            array (
                'id' => 53,
                'name' => 'LIMITED EDITION CHECK PRINTED SKIRT',
                'description' => 'Long skirt made with flowing fabric. Frayed contrasting trim detailing and invisible side zip fastening.',
                'imgPath' => 'images/1588590934.jpg',
                'isActive' => 1,
                'category_id' => 4,
                'created_at' => '2020-05-04 11:15:34',
                'updated_at' => '2020-05-04 11:15:34',
            ),
            53 => 
            array (
                'id' => 54,
                'name' => 'BELTED RUSTIC SKIRT',
                'description' => 'High-waist skirt in a linen blend. Zip hidden in the seam. Belt detail with buckle lined in the same fabric.',
                'imgPath' => 'images/1588590972.jpg',
                'isActive' => 1,
                'category_id' => 4,
                'created_at' => '2020-05-04 11:16:12',
                'updated_at' => '2020-05-04 11:16:12',
            ),
            54 => 
            array (
                'id' => 55,
                'name' => 'RUFFLED DRESS',
                'description' => 'V-neck mini dress featuring thin straps and ruffle trims.',
                'imgPath' => 'images/1588591509.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:25:09',
                'updated_at' => '2020-05-04 11:25:09',
            ),
            55 => 
            array (
                'id' => 56,
                'name' => 'LOOSE-FITTING TEXTURED DRESS',
                'description' => 'Strappy dress with a straight-cut neckline. Featuring interior lining, a ruffled hem and invisible zip fastening on the back.',
                'imgPath' => 'images/1588591557.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:25:57',
                'updated_at' => '2020-05-04 11:25:57',
            ),
            56 => 
            array (
                'id' => 57,
                'name' => 'DRAPED DRESS',
                'description' => 'Collared dress with long sleeves. Featuring a draped waist with pleats, a front slit and invisible side zip fastening.',
                'imgPath' => 'images/1588591727.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:28:47',
                'updated_at' => '2020-05-04 11:28:47',
            ),
            57 => 
            array (
                'id' => 58,
                'name' => 'FLORAL PRINT DRESS',
                'description' => 'Short dress featuring a V-neckline, short puff sleeves, gathered detail, ruffled hem and invisible zip fastening.',
                'imgPath' => 'images/1588591839.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:30:39',
                'updated_at' => '2020-05-04 11:30:39',
            ),
            58 => 
            array (
                'id' => 59,
                'name' => 'DRAPED POLKA DOT DRESS',
                'description' => 'Short dress featuring a plunging V-neckline with ties. Short puff sleeves with elasticated cuffs. Draped fabric detailing.',
                'imgPath' => 'images/1588591898.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:31:38',
                'updated_at' => '2020-05-04 11:31:38',
            ),
            59 => 
            array (
                'id' => 60,
                'name' => 'DRESS WITH CUTWORK EMBROIDERY',
                'description' => 'High neck dress with sleeves falling below the elbow and elastic trim. Featuring cutwork embroidery detailing and a buttoned opening in the back.',
                'imgPath' => 'images/1588591943.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:32:23',
                'updated_at' => '2020-05-04 11:32:23',
            ),
            60 => 
            array (
                'id' => 61,
                'name' => 'MULTICOLOURED KNIT TUNIC',
                'description' => 'Knit tunic featuring a slot collar with tie detail, short sleeves and side vents at the hem.',
                'imgPath' => 'images/1588592014.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:33:34',
                'updated_at' => '2020-05-04 11:33:34',
            ),
            61 => 
            array (
                'id' => 62,
                'name' => 'CONTRAST TULLE DRESS',
                'description' => 'Short dress featuring a slot collar with tied detail. Gathered detail and ruffled hem in matching fabric.',
                'imgPath' => 'images/1588592082.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:34:42',
                'updated_at' => '2020-05-04 11:34:42',
            ),
            62 => 
            array (
                'id' => 63,
                'name' => 'VOLUMINOUS RUSTIC DRESS',
                'description' => 'Midi dress made of a linen blend. Features a V-neckline and puff sleeves falling below the elbow with elastic cuff and an invisible back zip fastening.',
                'imgPath' => 'images/1588592195.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:36:35',
                'updated_at' => '2020-05-04 11:36:35',
            ),
            63 => 
            array (
                'id' => 64,
                'name' => 'STRIPED SHIRT DRESS',
                'description' => 'Collared dress with long cuffed sleeves. Side vents at the hem. Button-up front.',
                'imgPath' => 'images/1588592307.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:38:27',
                'updated_at' => '2020-05-04 11:38:27',
            ),
            64 => 
            array (
                'id' => 65,
                'name' => 'POPLIN SHIRT DRESS',
                'description' => 'Collared dress featuring long sleeves, a belt with lined buckle and button-up front.  CHOOSE A SIZE',
                'imgPath' => 'images/1588592349.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:39:09',
                'updated_at' => '2020-05-04 11:39:09',
            ),
            65 => 
            array (
                'id' => 66,
                'name' => 'GREEN FLORAL PRINT DRESS',
                'description' => 'Midi dress with a round neckline and puff sleeves falling below the elbow. Front button fastening.',
                'imgPath' => 'images/1588592393.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:39:53',
                'updated_at' => '2020-05-04 11:39:53',
            ),
            66 => 
            array (
                'id' => 67,
                'name' => 'KNOTTED DRESS',
                'description' => 'V-neck dress with thin straps, front knot detail and an asymmetric hem.',
                'imgPath' => 'images/1588592442.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:40:42',
                'updated_at' => '2020-05-04 11:40:42',
            ),
            67 => 
            array (
                'id' => 68,
                'name' => 'CHECK DRESS',
                'description' => 'Midi dress with a straight neckline, thin straps and pleat detail on the waist.',
                'imgPath' => 'images/1588592502.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:41:42',
                'updated_at' => '2020-05-04 11:41:42',
            ),
            68 => 
            array (
                'id' => 69,
                'name' => 'PLEATED DRESS WITH BELT',
                'description' => 'V-neck midi dress with short sleeves. Belt with metal buckle. Lining.  CHOOSE A SIZE',
                'imgPath' => 'images/1588592657.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:44:17',
                'updated_at' => '2020-05-04 11:44:17',
            ),
            69 => 
            array (
                'id' => 70,
                'name' => 'BLAZER-STYLE JUMPSUIT',
                'description' => 'Playsuit with a shawl collar and short sleeves with puffy shoulders. Crossover V-neckline. Inside zip fastening at the front and tied detail to one side.',
                'imgPath' => 'images/1588592726.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:45:26',
                'updated_at' => '2020-05-04 11:45:26',
            ),
            70 => 
            array (
                'id' => 71,
                'name' => 'RUFFLED KNIT DRESS',
                'description' => 'Knit sleeveless dress with a V-neckline and ruffle trims.',
                'imgPath' => 'images/1588592815.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:46:55',
                'updated_at' => '2020-05-04 11:46:55',
            ),
            71 => 
            array (
                'id' => 72,
                'name' => 'ORANGE RUFFLED DRESS',
                'description' => 'Strappy dress featuring a round neck, elastic detail at the back and ruffled hem.',
                'imgPath' => 'images/1588592854.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:47:34',
                'updated_at' => '2020-05-04 11:47:34',
            ),
            72 => 
            array (
                'id' => 73,
                'name' => 'MIDI DRESS WITH DRAPED DETAIL',
                'description' => 'Strapless dress with a straight neckline. Featuring inner side elastic detail, a front knot, side vent at the hem and invisible back zip fastening.',
                'imgPath' => 'images/1588592897.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:48:17',
                'updated_at' => '2020-05-04 11:48:17',
            ),
            73 => 
            array (
                'id' => 74,
                'name' => 'ASYMMETRIC MIDI DRESS',
                'description' => 'Round neck dress with long asymmetric sleeves. Featuring gathered elastic sides.',
                'imgPath' => 'images/1588592938.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:48:58',
                'updated_at' => '2020-05-04 11:48:58',
            ),
            74 => 
            array (
                'id' => 75,
                'name' => 'LONG SHIRT WITH VENTS',
                'description' => 'Collared shirt with long sleeves. Hem with side vents and a button-up front.',
                'imgPath' => 'images/1588592976.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:49:36',
                'updated_at' => '2020-05-04 11:49:36',
            ),
            75 => 
            array (
                'id' => 76,
                'name' => 'FLOWER SHIRT DRESS',
                'description' => 'Midi dress with a shirt collar and long cuffed sleeves. Featuring a tied crossover belt at the front, side vents at the hem and front button fastening.',
                'imgPath' => 'images/1588593042.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:50:42',
                'updated_at' => '2020-05-04 11:50:42',
            ),
            76 => 
            array (
                'id' => 77,
                'name' => 'BLUE/BLACK CHECK DRESS',
                'description' => 'V-neck midi dress with short puff sleeves and pleated detail.',
                'imgPath' => 'images/1588593090.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:51:30',
                'updated_at' => '2020-05-04 11:51:30',
            ),
            77 => 
            array (
                'id' => 78,
                'name' => 'POPLIN DRESS',
                'description' => 'Midi dress featuring a round neckline, sleeves falling below the elbow, a ruffled hem and a buttoned opening at the back.',
                'imgPath' => 'images/1588593173.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:52:53',
                'updated_at' => '2020-05-04 11:52:53',
            ),
            78 => 
            array (
                'id' => 79,
                'name' => 'KHAKI SATIN DRESS',
                'description' => 'Short dress with a cowl neck and thin straps.',
                'imgPath' => 'images/1588593222.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:53:42',
                'updated_at' => '2020-05-04 11:53:42',
            ),
            79 => 
            array (
                'id' => 80,
                'name' => 'WHITE DRESS WITH CUTWORK EMBROIDERY',
                'description' => 'Short dress with a round neck and sleeves falling below the elbow. Featuring lining, matching openwork embroidery fabric detail and button-up front.',
                'imgPath' => 'images/1588593277.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:54:37',
                'updated_at' => '2020-05-04 11:54:37',
            ),
            80 => 
            array (
                'id' => 81,
                'name' => 'PRINTED POPLIN DRESS',
                'description' => 'Midi dress with round neckline, ruffled cap sleeves and elastic trims.',
                'imgPath' => 'images/1588593301.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:55:01',
                'updated_at' => '2020-05-04 11:55:01',
            ),
            81 => 
            array (
                'id' => 82,
                'name' => 'POLKA PLEATED DRESS WITH BELT',
                'description' => 'Short dress featuring a slot collar with tie detail, sleeves falling below the elbow with elastic trim and tie belt in contrast fabric.',
                'imgPath' => 'images/1588593350.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:55:50',
                'updated_at' => '2020-05-04 11:55:50',
            ),
            82 => 
            array (
                'id' => 83,
                'name' => 'DOTTED MESH DRESS WITH BELT',
                'description' => 'Dress featuring a collar with tie fastening and crossover V-neck, sleeves falling below the elbow, elastic waist, belt with contrast metal buckle and ruffled hem.',
                'imgPath' => 'images/1588593431.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:57:11',
                'updated_at' => '2020-05-04 11:57:11',
            ),
            83 => 
            array (
                'id' => 84,
                'name' => 'TWEED DRESS',
                'description' => 'Dress featuring a straight neckline, wide straps, false gem button details and invisible side zip fastening.',
                'imgPath' => 'images/1588593476.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:57:56',
                'updated_at' => '2020-05-04 11:57:56',
            ),
            84 => 
            array (
                'id' => 85,
                'name' => 'JACQUARD DRESS WITH RUFFLED TRIM',
                'description' => 'Sleeveless knit dress with an asymmetric neckline. Ruffled detail.',
                'imgPath' => 'images/1588593541.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2020-05-04 11:59:01',
                'updated_at' => '2020-05-04 11:59:01',
            ),
        ));
        
        
    }
}