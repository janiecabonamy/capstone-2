<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Blouses',
                'imgPath' => 'images/1588581911.jpeg',
                'isActive' => 1,
                'categorycode' => 'BLOUSE',
                'created_at' => '2020-05-04 08:45:11',
                'updated_at' => '2020-05-05 01:42:30',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Knitwear',
                'imgPath' => 'images/1588583996.jpeg',
                'isActive' => 1,
                'categorycode' => 'KNITWEAR',
                'created_at' => '2020-05-04 09:19:56',
                'updated_at' => '2020-05-04 22:06:17',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Jeans',
                'imgPath' => 'images/1588585805.jpeg',
                'isActive' => 1,
                'categorycode' => 'JEANS',
                'created_at' => '2020-05-04 09:50:05',
                'updated_at' => '2020-05-04 09:50:05',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Skirts',
                'imgPath' => 'images/1588590144.jpeg',
                'isActive' => 1,
                'categorycode' => 'SKIRT',
                'created_at' => '2020-05-04 11:02:24',
                'updated_at' => '2020-05-04 11:02:24',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Dresses',
                'imgPath' => 'images/1588591475.jpeg',
                'isActive' => 1,
                'categorycode' => 'DRESS',
                'created_at' => '2020-05-04 11:24:35',
                'updated_at' => '2020-05-04 11:24:35',
            ),
        ));
        
        
    }
}