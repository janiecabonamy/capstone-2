<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Admin Official',
                'email' => 'admin@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$1ctknaYUvnNlfpmiZnCRROeYfE/ghOXq/i3jxBgGbFdY0VerXBTSm',
                'role_id' => 1,
                'remember_token' => NULL,
                'created_at' => '2020-05-04 08:41:30',
                'updated_at' => '2020-05-04 08:41:30',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Test User',
                'email' => 'user@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$5W6ei4Ntf2bUDSUU1icox.GH72ZDrqxzpzUnQ4S8QWOtsZYgbv91q',
                'role_id' => 2,
                'remember_token' => NULL,
                'created_at' => '2020-05-04 09:58:16',
                'updated_at' => '2020-05-04 09:58:16',
            ),
        ));
        
        
    }
}