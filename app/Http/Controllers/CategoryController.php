<?php

namespace App\Http\Controllers;

use App\Category;
use App\Asset;
use App\Inventory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $assets = Asset::all();
       $inventories = Inventory::all();
        
       return view('categories.index')->with('categories', $categories)->with('assets', $assets)->with('inventories', $inventories);

        

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Category::class);
               $categories = Category::all();
               return view('categories.create')->with('categories', $categories);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //dd($request->input('image'));

        //implement authorization via LaraveL Policy
        $this->authorize('create', Category::class);

        // retrieve the property named category from the FormData object sent via JS Fetch and save it as a variable named $name
        $request->validate([
            'catName' => 'required|string|unique:categories,name',
            'catImage' => 'required|image',
            'catCode' => 'required|string'
            
        ]);

        $catName = htmlspecialchars($request->input('catName'));
        $catImage = $request->file('catImage');
        $catCode = htmlspecialchars($request->input('catCode'));


        $category = new Category;

        $category->name = $catName;

        $file_name = time() . "." . $catImage->getClientOriginalExtension();
        $destination = "images/";
        $catImage->move($destination, $file_name);
        $category->imgPath = $destination.$file_name;

        $category->categorycode = $catCode;

        $category->save();

        // redirect back to catalogue
        return redirect('/categories/create');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category, Asset $asset)
    {
        //dd($category->id);

        $categories = Category::all();
        $assets = Asset::all();
        $inventories = Inventory::all();

         $freesize[]=0;
         $extrasmall[]=0;
         $small[]=0;
         $medium[]=0;
         $large[]=0;
         $extralarge[]=0;

         //baliktad. for each inventories as inventory dapat???
        foreach($assets as $asset){
          $fnum=0;
          $xsnum=0;
          $snum=0;
          $mnum=0;
          $lnum=0;
          $xlnum=0;
          if($category->id == $asset->category_id){ 
            foreach($inventories as $inventory){
              if($asset->id == $inventory->asset_id && $inventory->isAvailable == 1){
                //dd($inventory->size);
                if($inventory->size == "freesize"){
                  $fnum++;
                }
                if($inventory->size == 'extra small'){
                  $xsnum++;
                }
                if($inventory->size == 'small'){
                  $snum++;
                }
                if($inventory->size == 'medium'){
                  $mnum++;
                }
                if($inventory->size == 'large'){
                  $lnum++;
                }
                if($inventory->size == 'extra large'){
                  $xlnum++;
                }
              }
            }
              $freesize[]=$fnum;
              $extrasmall[]=$xsnum;
              $small[]=$snum;
              $medium[]=$mnum;
              $large[]=$lnum;
              $extralarge[]=$xlnum;
          }
        }


        $countA = 1;
        $countB = 1;
        $countC = 1;
        $countD = 1;
        $countE = 1;
        $countF = 1;

        $freeavail[]=0;
        $xsmallavail[]=0;
        $smallavail[]=0;
        $mediumavail[]=0;
        $largeavail[]=0;
        $xlargeavail[]=0;

        foreach($assets as $asset){
        $favail = Inventory::where('size', '=','freesize')
        ->where('asset_id','=', $asset->id)
        ->where('isAvailable', '=', 1)
        ->get();

        $freeavail[]= $favail;

        $xsavail = Inventory::where('size', '=','extra small')
        ->where('asset_id','=', $asset->id)
        ->where('isAvailable', '=', 1)
        ->get();

         $xsmallavail[]= $xsavail;

        $savail = Inventory::where('size', '=','small')
        ->where('asset_id','=', $asset->id)
        ->where('isAvailable', '=', 1)
        ->get();

         $smallavail[]= $savail;

        $mavail = Inventory::where('size', '=','medium')
        ->where('asset_id','=', $asset->id)
        ->where('isAvailable', '=', 1)
        ->get();

         $mediumavail[]= $mavail;

        $lavail = Inventory::where('size', '=','large')
        ->where('asset_id','=', $asset->id)
        ->where('isAvailable', '=', 1)
        ->get();

         $largeavail[]= $lavail;

        $xlavail = Inventory::where('size', '=','extra large')
        ->where('asset_id','=', $asset->id)
        ->where('isAvailable', '=', 1)
        ->get();
        
         $xlargeavail[]= $xlavail;
        }

        //dd(count($xlargeavail[3]));
        
        return view('assets.index')->with('categories', $categories)->with('assets', $assets)->with('inventories', $inventories)->with('category', $category)
        ->with('freesize', $freesize)
        ->with('extrasmall', $extrasmall)
        ->with('small', $small)
        ->with('medium', $medium)
        ->with('large', $large)
        ->with('extralarge', $extralarge)
        ->with('countA', $countA)
        ->with('countB', $countB)
        ->with('countC', $countC)
        ->with('countD', $countD)
        ->with('countE', $countE)
        ->with('countF', $countF)
        ->with('freeavail', $freeavail)
        ->with('xsmallavail', $xsmallavail)
        ->with('smallavail', $smallavail)
        ->with('mediumavail', $mediumavail)
        ->with('largeavail', $largeavail)
        ->with('xlargeavail', $xlargeavail);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category, Asset $asset)
    {
        //dd($category);
        $categories = Category::all();
       $assets = Asset::all();
       $inventories = Inventory::all();

        $this->authorize('update', Asset::class);
      
       return view('categories.edit')->with('categories', $categories)->with('assets', $assets)->with('inventories', $inventories)->with('category', $category)->with('asset', $asset);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $this->authorize('update', Category::class);
      $request->validate([
          'name' => 'required|string',
          'image' => 'image'
      ]);

      $name = htmlspecialchars($request->input('name'));
      $duplicate = Category::where(strtolower('name'), strtolower($name))->first();
      if ($duplicate == null) {
          
      }
      $category->name = $name;

      // if an image file upload is found, replace the current image of the product with the new upload
      if ($request->file('image') != null) {
          $image = $request->file('image');
          // set the file name of the uploaded image to be the time of upload, retaining the original file type
          $file_name = time() . "." . $image->getClientOriginalExtension();
          // set target destination where the file will be saved in
          $destination = "images/";
          // call the move() method of the $image object to save the uploaded file in the target destination under the specified file name
          $image->move($destination, $file_name);
          // set the path of the saved image as the value for the column img_path of this record
          $category->imgPath = $destination.$file_name;
      }

      $category->save();

      

      return redirect("/categories/"."$category->category_id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category, Asset $asset, Inventory $inventory)
    {

      $categories = Category::all();
       $assets = Asset::all();
       $inventories = Inventory::all();


        if($category->isActive == 1){
          $category->isActive = 0;
          foreach($assets as $asset){
            if($asset->category_id == $category->id){
              $asset->isActive = 0;
              $asset->save();
              foreach($inventories as $inventory){
                if($inventory->asset_id == $asset->id){
                  $inventory->isAvailable = 0;
                  $inventory->save();
                }
              }
            }
          }
        }else{
          $category->isActive = 1;

          foreach($assets as $asset){
            if($asset->category_id == $category->id){
              $asset->isActive = 1;
              $asset->save();
              foreach($inventories as $inventory){
                if($inventory->asset_id == $asset->id){
                  $inventory->isAvailable = 1;
                  $inventory->save();
                }
              }
            }
          }
        }
        $category->save();
        return redirect('/categories');
    }
}
