<?php

namespace App\Http\Controllers;

use App\Category;
use App\Asset;
use App\Inventory;
use Illuminate\Http\Request;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $assets = Asset::all();
        $inventories = Inventory::all();

        return view('inventory.index')->with('categories', $categories)->with('assets', $assets)->with('inventories', $inventories)->with('category', $category);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function show(Inventory $inventory)
    {
        $categories = Category::all();
        $assets = Asset::all();
        $inventories = Inventory::all();

        return view('inventories.show')->with('categories', $categories)->with('assets', $assets)->with('inventories', $inventories);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Inventory  $inventory           

     * @return \Illuminate\Http\Response
     */
    public function edit(Inventory $inventory, Asset $asset)
    {

        //dd($inventory);
        $categories = Category::all();
       $assets = Asset::all();
       $inventories = Inventory::all();

        $this->authorize('update', Inventory::class);
      
       return view('inventories.edit')->with('categories', $categories)->with('assets', $assets)->with('inventory', $inventory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inventory $inventory)
    {
        $this->authorize('update', Inventory::class);
      $request->validate([
          'name' => 'string',
          'size' => 'string'
      ]);

      $name = htmlspecialchars($request->input('name'));
      $size = htmlspecialchars($request->input('size'));
      //$category = htmlspecialchars($request->input('category'));

      $duplicate = Asset::where(strtolower('name'), strtolower($name))->first();
      if ($duplicate == null) {
          
      }
      // $product is the product object to be edited, this was obtained via LaraveL's route-model binding
      // overwrite the properties of $product with the input values from the edit form
      $inventory->name = $name;
      $inventory->size = $size;
      //$inventory->category_id = $category;


      $inventory->save();

      

      return redirect("/assets/"."$inventory->asset_id");

    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */

    public function destroy(Inventory $inventory)
    {
        if($inventory->isAvailable == 1){
          $inventory->isAvailable = 0;
        }else{
          $inventory->isAvailable = 1;
        }
        $inventory->save();

        return redirect('/assets/'.$inventory->asset_id);
    
    }
}
