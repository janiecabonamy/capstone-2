<?php

namespace App\Http\Controllers;

use App\User;
use App\Category;
use App\Asset;
use App\Inventory;
use App\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       $categories = Category::all();
       $assets = Asset::all();
       //$transactions = Transaction::where('asset_id', $transaction->id)->get();
       $transactions = Transaction::all();
       //dd($transactions);


       /*$freeassign[] = 0;
       $xsassign[]=0;
       $sassign[]=0;
       $massign[]=0;
       $lassign[]=0;
       $xlassign[]=0;*/

       //dd($transactions);

        /*foreach($transactions as $transaction){*/
            //foreach($assets as $asset){
                //if($asset->id == $transaction->asset_id){


            //dd($transaction->asset_id);
            //if($transaction->asset->inventory->size->isAvailable){
           /* if($transaction->status_id == 1){
            $invassign[] = Inventory::where('asset_id', '=', $transaction->asset_id)
            ->where('isAvailable','=', 1)
            ->where('size','=', $transaction->size)
            ->first();*/

                   /*$freeassign[] = Inventory::where('asset_id', '=', $transaction->asset_id)
                   ->where('isAvailable','=', 1)
                   ->where('size','=', 'size')
                   ->first();*/

                   //dd($asset);
                   //dd($transaction->size);

                   /*$xsassign[] = Inventory::where('asset_id', '=', $transaction->asset_id)
                   ->where('isAvailable','=', 1)
                   ->where('size','=', 'extra small')
                   ->first();*/

                   //dd($xsassign[1]);

                   /*$sassign[] = Inventory::where('asset_id', '=', $transaction->asset_id)
                   ->where('isAvailable','=', 1)
                   ->where('size','=', 'small')
                   ->first();

                   $massign[] = Inventory::where('asset_id', '=', $transaction->asset_id)
                   ->where('isAvailable','=', 1)
                   ->where('size','=', 'medium')
                   ->first();*/

                   //dd($massign);
                   /*if($massign == null){
                        $massign = 0;
                   }else{
                        $massign = $massign->id;
                   }*/

                   /*$lassign[] = Inventory::where('asset_id', '=', $transaction->asset_id)
                   ->where('isAvailable','=', 1)
                   ->where('size','=', 'large')
                   ->first();

                   $xlassign[] = Inventory::where('asset_id', '=', $transaction->asset_id)
                   ->where('isAvailable','=', 1)
                   ->where('size','=', 'extra large')
                   ->first();*/

                   //dd($sassign->first());
            //}
          //}      
            //}
               /*}
        }*/
        //dd($freeassign[1]->id);
        //dd($sassign[3]->id);
        //dd($freeassign);
    //dd($invassign);
      /* $countA = 0;*/
      
       
   return view('transactions.index')->with('transactions', $transactions)->with('assets', $assets)->with('categories', $categories);


        /*->with('invassign', $invassign)
        ->with('countA', $countA);*/



      /* ->with('freeassign', $freeassign)
       ->with('xsassign', $xsassign)
       ->with('sassign', $sassign)
       ->with('massign', $massign)
       ->with('lassign', $lassign)
       ->with('xlassign', $xlassign)
       ->with('assetassign', $assetassign)
       ->with('countA', $countA);*/

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //dd($transaction);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return                              <td>{{$transaction->status_id}}</td>
 \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $categories = Category::all();
       $assets = Asset::all();
       $transactions = Transaction::all();

        //implement authorization via LaraveL Policy
        $this->authorize('create', Transaction::class);

        // retrieve the property named category from the FormData object sent via JS Fetch and save it as a variable named $name
        $request->validate([
            'size' => 'required|string',
            'borrowdate' => 'required|date',
            'returndate' => 'required|date'
            
        ]);

        $username = htmlspecialchars($request->input('userid'));
        $assetname = htmlspecialchars($request->input('assetid'));
        $size = htmlspecialchars($request->input('size'));
        $returndate = htmlspecialchars($request->input('returndate'));
        $borrowdate = htmlspecialchars($request->input('borrowdate'));

        //dd($request->input('userid'));


        $transaction = new Transaction;

        $transaction->user_id = $username;
        $transaction->asset_id = $assetname;
        $transaction->size = $size;
        $transaction->status_id = 1;


        $transaction->borrowDate = $borrowdate;
        $transaction->returnDate = $returndate;

        $transaction->save();

        // redirect back to catalogue
        return redirect('/transactions')->with('transactions', $transactions)->with('assets', $assets)->with('categories', $categories);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Asset $transaction)
    {//$transaction above means data from the Asset Database/Model
    //$transaction came from the passed through attribute action = '/transaction/{{asset->id}}' from my asset index.blade.php going to the TransactionController.php
      //dd("hi");
       $categories = Category::all();
       $assets = Asset::all();
       $transactions = Transaction::where('asset_id', $transaction->id)->get();

       $freeassign = Inventory::where('asset_id', '=', $transaction->id)
       ->where('isAvailable','=', 1)
       ->where('size','=', 'freesize')
       ->first();

       $xsassign = Inventory::where('asset_id', '=', $transaction->id)
       ->where('isAvailable','=', 1)
       ->where('size','=', 'extra small')
       ->first();

       $sassign = Inventory::where('asset_id', '=', $transaction->id)
       ->where('isAvailable','=', 1)
       ->where('size','=', 'small')
       ->first();

       $massign = Inventory::where('asset_id', '=', $transaction->id)
       ->where('isAvailable','=', 1)
       ->where('size','=', 'medium')
       ->first();

       $lassign = Inventory::where('asset_id', '=', $transaction->id)
       ->where('isAvailable','=', 1)
       ->where('size','=', 'large')
       ->first();

       $xlassign = Inventory::where('asset_id', '=', $transaction->id)
       ->where('isAvailable','=', 1)
       ->where('size','=', 'extra large')
       ->first();

       //dd($xlassign->id);
       //dd($categories);


       return view('transactions.show')->with('transactions', $transactions)->with('assets', $assets)->with('categories', $categories)
       ->with('freeassign', $freeassign)
       ->with('xsassign', $xsassign)
       ->with('sassign', $sassign)
       ->with('massign', $massign)
       ->with('lassign', $lassign)
       ->with('xlassign', $xlassign);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
         $this->authorize('update', Transaction::class);
         $request->validate([
            'status' => 'required',
            'view' => 'string'
        ]);

        $status = $request->input('status');
        $view = $request->input('view');
        //dd($status);
        if($status == 4){
            $invassign = Inventory::where('asset_id', '=', $transaction->asset_id)
            ->where('isAvailable','=', 1)
            ->where('size','=', $transaction->size)
            ->pluck('id')->toArray();
            //dd($invassign[0]);
            //dd(count($invassign));
            if(count($invassign) > 0 ){
                $inventory = Inventory::where('id', '=', $invassign[0])->first();
                //dd($inventory);
                $inventory->isAvailable = 0;
                $inventory->save();
                $transaction->status_id = 4;
                $transaction->assigned = $inventory->id;
                $transaction->save();        
            }else{
                $transaction->status_id = 3;
                $transaction->save();
                //return "Out of Stock";
            }
        }elseif($status == 3){
            $transaction->status_id = 3;
            $transaction->save();
            //return "Request denied";
        }elseif($status == 2){
            $transaction->status_id = 2;
            $transaction->save();
            //return "Your reservation is canceled";
        }elseif($status == 5){

            $transaction->status_id = 5;
            $transaction->save();

            $inventory = Inventory::where('id', '=' , $transaction->assigned)->first();
            //dd($inventory);
            $inventory->isAvailable = 1;
                $inventory->save();
            //return "Product successfully renturned";
        }  

        if($view == "index"){          
            return redirect('/transactions');
        }elseif($view == "show"){
            return redirect("/transactions/"."$transaction->asset_id");
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }
}
