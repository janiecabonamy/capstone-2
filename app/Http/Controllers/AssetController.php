<?php

namespace App\Http\Controllers;

use App\Category;
use App\Asset;
use App\Inventory;
use Illuminate\Http\Request;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assets = Asset::all();
       $inventories = Inventory::all();
        
       $freesize[]=0;
         $extrasmall[]=0;
         $small[]=0;
         $medium[]=0;
         $large[]=0;
         $extralarge[]=0;

         //baliktad. for each inventories as inventory dapat???
        foreach($assets as $asset){
          $fnum=0;
          $xsnum=0;
          $snum=0;
          $mnum=0;
          $lnum=0;
          $xlnum=0;
         
            foreach($inventories as $inventory){
              if($asset->id == $inventory->asset_id && $inventory->isAvailable == 1){
                //dd($inventory->size);
                if($inventory->size == "freesize"){
                  $fnum++;
                }
                if($inventory->size == 'extra small'){
                  $xsnum++;
                }
                if($inventory->size == 'small'){
                  $snum++;
                }
                if($inventory->size == 'medium'){
                  $mnum++;
                }
                if($inventory->size == 'large'){
                  $lnum++;
                }
                if($inventory->size == 'extra large'){
                  $xlnum++;
                }
              }
            }
              $freesize[]=$fnum;
              $extrasmall[]=$xsnum;
              $small[]=$snum;
              $medium[]=$mnum;
              $large[]=$lnum;
              $extralarge[]=$xlnum;
          
        }


        $countA = 1;
        $countB = 1;
        $countC = 1;
        $countD = 1;
        $countE = 1;
        $countF = 1;

        $freeavail[]=0;
        $xsmallavail[]=0;
        $smallavail[]=0;
        $mediumavail[]=0;
        $largeavail[]=0;
        $xlargeavail[]=0;

        foreach($assets as $asset){
        $favail = Inventory::where('size', '=','freesize')
        ->where('asset_id','=', $asset->id)
        ->where('isAvailable', '=', 1)
        ->get();

        $freeavail[]= $favail;

        $xsavail = Inventory::where('size', '=','extra small')
        ->where('asset_id','=', $asset->id)
        ->where('isAvailable', '=', 1)
        ->get();

         $xsmallavail[]= $xsavail;

        $savail = Inventory::where('size', '=','small')
        ->where('asset_id','=', $asset->id)
        ->where('isAvailable', '=', 1)
        ->get();

         $smallavail[]= $savail;

        $mavail = Inventory::where('size', '=','medium')
        ->where('asset_id','=', $asset->id)
        ->where('isAvailable', '=', 1)
        ->get();

         $mediumavail[]= $mavail;

        $lavail = Inventory::where('size', '=','large')
        ->where('asset_id','=', $asset->id)
        ->where('isAvailable', '=', 1)
        ->get();

         $largeavail[]= $lavail;

        $xlavail = Inventory::where('size', '=','extra large')
        ->where('asset_id','=', $asset->id)
        ->where('isAvailable', '=', 1)
        ->get();
        
         $xlargeavail[]= $xlavail;
        }

        //dd(count($xlargeavail[3]));
        
        return view('assets.assets')->with('assets', $assets)->with('inventories', $inventories)
        ->with('freesize', $freesize)
        ->with('extrasmall', $extrasmall)
        ->with('small', $small)
        ->with('medium', $medium)
        ->with('large', $large)
        ->with('extralarge', $extralarge)
        ->with('countA', $countA)
        ->with('countB', $countB)
        ->with('countC', $countC)
        ->with('countD', $countD)
        ->with('countE', $countE)
        ->with('countF', $countF)
        ->with('freeavail', $freeavail)
        ->with('xsmallavail', $xsmallavail)
        ->with('smallavail', $smallavail)
        ->with('mediumavail', $mediumavail)
        ->with('largeavail', $largeavail)
        ->with('xlargeavail', $xlargeavail);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    
    public function store(Request $request)
    {
        //dd($request->input('image'));

        //dd($asset);

        //implement authorization via LaraveL Policy
        $this->authorize('create', Asset::class);

        // retrieve the property named category from the FormData object sent via JS Fetch and save it as a variable named $name
        $request->validate([
            'prodTitle' => 'required|string|unique:assets,name',
            'prodImage' => 'required|image',
            'prodDesc' => 'required|string',
            'freeQty' => 'min:0',
            'xsQty' => 'min:0',
            'sQty' => 'min:0',
            'mQty' => 'min:0',
            'lQty' => 'min:0',
            'xlQty' => 'min:0'
            
        ]);

        $prodTitle = htmlspecialchars($request->input('prodTitle'));
        $prodImage = $request->file('prodImage');
        $prodDesc = htmlspecialchars($request->input('prodDesc'));
        $category = htmlspecialchars($request->input('category'));
        $freeQty = $request->input('freeQty');
        $xsQty = $request->input('xsQty');
        $sQty = $request->input('sQty');
        $mQty = $request->input('mQty');
        $lQty = $request->input('lQty');
        $xlQty = $request->input('xlQty');

        $asset = new Asset;

        $asset->name = $prodTitle;

        $file_name = time() . "." . $prodImage->getClientOriginalExtension();
        $destination = "images/";
        $prodImage->move($destination, $file_name);
        $asset->imgPath = $destination.$file_name;

        $asset->description = $prodDesc;
        $asset->category_id = $category;


        $asset->save();
        $asset_id = $asset->id;


        for ($counter ; $freeQty > 0 ; $freeQty--){
            $inventory = new Inventory;       
            $inventory->name = $prodTitle;
            $inventory->size = "freesize";
            $inventory->asset_id = $asset_id;
            $inventory->inventory_id = $asset_id . "-fr-" . $freeQty;
            $inventory->description = $prodDesc;
            $inventory->save();
        };


        for ($counter ; $xsQty > 0 ; $xsQty--){
            $inventory = new Inventory;       
            $inventory->name = $prodTitle;
            $inventory->size = "extra small";
            $inventory->asset_id = $asset_id;
            $inventory->inventory_id = $asset_id . "-xs-" . $xsQty;
            $inventory->description = $prodDesc;
            $inventory->save();
        };

        for ($counter ; $sQty > 0 ; $sQty--){
            $inventory = new Inventory;       
            $inventory->name = $prodTitle;
            $inventory->size = "small";
            $inventory->asset_id = $asset_id;
            $inventory->inventory_id = $asset_id . "-sm-" . $sQty;
            $inventory->description = $prodDesc;
            $inventory->save();
        };

        for ($counter ; $mQty > 0 ; $mQty--){
            $inventory = new Inventory;       
            $inventory->name = $prodTitle;
            $inventory->size = "medium";
            $inventory->asset_id = $asset_id;
            $inventory->inventory_id = $asset_id . "-md-" . $mQty;
            $inventory->description = $prodDesc;
            $inventory->save();
        };

        for ($counter ; $lQty > 0 ; $lQty--){
            $inventory = new Inventory;       
            $inventory->name = $prodTitle;
            $inventory->size = "large";
            $inventory->asset_id = $asset_id;
            $inventory->inventory_id = $asset_id . "-lg-" . $lQty;
            $inventory->description = $prodDesc;
            $inventory->save();
        };

        for ($counter ; $xlQty > 0 ; $xlQty--){
            $inventory = new Inventory;       
            $inventory->name = $prodTitle;
            $inventory->size = "extra large";
            $inventory->asset_id = $asset_id;
            $inventory->inventory_id = $asset_id . "-xl-" . $xlQty;
            $inventory->description = $prodDesc;
            $inventory->save();
        };


        // redirect back to categories
        return redirect('/categories/create');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function show(Asset $asset)
    {
        $categories = Category::all();
        $assets = Asset::all();
        $inventories = Inventory::all();

      //dd($category->assets);
        return view('inventories.index')->with('asset', $asset)->with('categories', $categories)->with('assets', $assets)->with('inventories', $inventories);

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function edit(Asset $asset)
    {
        // die dump the name of the product to be edited
        // this works due to route-model binding
        //dd($asset->name);
       // use the authorize() controller helper to call the update method in the AssetPolicy

        //dd($asset);

        $categories = Category::all();
       $assets = Asset::all();
       $inventories = Inventory::all();

        $this->authorize('update', Asset::class);
      
       return view('assets.edit')->with('categories', $categories)->with('asset', $asset)->with('inventories', $inventories);

       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asset $asset)
    {
        $this->authorize('update', Asset::class);
      $request->validate([
          'name' => 'required|string',
          'description' => 'required|string',
          'size' => 'string',
          'category' => 'required',
          'image' => 'image'
      ]);

      $name = htmlspecialchars($request->input('name'));
      $description = htmlspecialchars($request->input('description'));
      $size = htmlspecialchars($request->input('size'));
      $category = htmlspecialchars($request->input('category'));

      $duplicate = Asset::where(strtolower('name'), strtolower($name))->first();
      if ($duplicate == null) {
          
      }
     
      $asset->name = $name;
      $asset->description = $description;
      $asset->category_id = $category;

      // if an image file upload is found, replace the current image of the product with the new upload
      if ($request->file('image') != null) {
          $image = $request->file('image');
          // set the file name of the uploaded image to be the time of upload, retaining the original file type
          $file_name = time() . "." . $image->getClientOriginalExtension();
          // set target destination where the file will be saved in
          $destination = "images/";
          // call the move() method of the $image object to save the uploaded file in the target destination under the specified file name
          $image->move($destination, $file_name);
          // set the path of the saved image as the value for the column img_path of this record
          $asset->imgPath = $destination.$file_name;
      }

      $asset->save();

      

      return redirect("/categories/"."$asset->category_id");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asset $asset, Category $category)
    {
          $categories = Category::all();
          $assets = Asset::all();
          $inventories = Inventory::all();

        if($asset->isActive == 1){
          $asset->isActive = 0;
        }else{
          $asset->isActive = 1;
        }
        $asset->save();

        return redirect('/categories/'.$asset->category_id);

    }
}
