<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function user()
    {
    	return $this->belongsTo('\App\User');
    }

    public function assets()
    {
    	return $this->belongsToMany('\App\Assets');
    }

    public function inventories()
    {
        return $this->belongsToMany('\App\Inventory');
    }

    public function status(){
        return $this->belongsTo('\App\Status');
    }
}
