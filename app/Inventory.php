<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    public function assets(){
    	return $this->belongsTo('\App\Asset');
    }

    public function transactions(){
    	return $this->hasMany('\App\Transactions');
    }
}
