<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'CategoryController@index')->name('category');

Route::resource('categories', 'CategoryController');
Route::resource('assets', 'AssetController');
Route::resource('inventories', 'InventoryController');
Route::resource('transactions', 'TransactionController');
Route::put('transactions/show', 'TransactionController@update');


//Route::resource('cart', 'CartController');

/*// define a route for creating a reservation item
Route::post('/cart', 'CartController@store');
// define a route for viewing the cart page
Route::get('/cart', 'CartController@index');
// define a route for updating a given product's cart quantity
Route::put('/cart/{id}', 'CartController@update');
// define a route for deleting a given product from the session cart variable
Route::delete('/cart/{id}', 'CartController@destroy');*/